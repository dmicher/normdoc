"НормДок" - текстовый редактор на русском языке для создания и поддержания массива нормативной документации в организаций.

Что желаю увидеть:

1. Файловая система для хранения нормативной информации;
2. Текстовый редакто для изменения хранимой информации;
3. Система терминов (включая различные словоформы);
4. Система требований однократного применения (цели, задачи);
5. Система требований многократного применения (нормы);
6. Контроль исполенния;
7. Система отслеживания версий;
8. Система пользователей;
9. Система обмена сообщениями и оповещениями.